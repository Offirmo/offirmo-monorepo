import { XError } from '@offirmo/common-error-fields';
declare function normalizeError(err_like?: Partial<Error>): XError;
export { normalizeError };
