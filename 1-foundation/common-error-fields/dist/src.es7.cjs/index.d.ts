import { XError } from "./types";
declare function create(): Set<string>;
declare const COMMON_ERROR_FIELDS: Set<string>;
export { XError, COMMON_ERROR_FIELDS, create };
