import { Logger, LogParams, createChildLogger } from '@offirmo/practical-logger-core';
declare function createLogger(p: LogParams): Logger;
export { createLogger, createChildLogger };
