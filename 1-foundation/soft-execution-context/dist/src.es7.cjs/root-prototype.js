"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
//import EventEmitter from './forks/emittery_0.3.0'
const emittery_1 = tslib_1.__importDefault(require("emittery"));
/////////////////////
const ROOT_PROTOTYPE = Object.create(null);
exports.ROOT_PROTOTYPE = ROOT_PROTOTYPE;
ROOT_PROTOTYPE.emitter = new emittery_1.default();
//# sourceMappingURL=root-prototype.js.map