"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const SUB_LIB = 'plugin/logical-stack';
exports.SUB_LIB = SUB_LIB;
const LOGICAL_STACK_BEGIN_MARKER = '';
exports.LOGICAL_STACK_BEGIN_MARKER = LOGICAL_STACK_BEGIN_MARKER;
const LOGICAL_STACK_END_MARKER = '';
exports.LOGICAL_STACK_END_MARKER = LOGICAL_STACK_END_MARKER;
const LOGICAL_STACK_SEPARATOR = '›';
exports.LOGICAL_STACK_SEPARATOR = LOGICAL_STACK_SEPARATOR;
// '⋅' '↘' ':' '•' '›'
const LOGICAL_STACK_MODULE_MARKER = '';
exports.LOGICAL_STACK_MODULE_MARKER = LOGICAL_STACK_MODULE_MARKER;
const LOGICAL_STACK_OPERATION_MARKER = '';
exports.LOGICAL_STACK_OPERATION_MARKER = LOGICAL_STACK_OPERATION_MARKER;
// '…'
const LOGICAL_STACK_SEPARATOR_NON_ADJACENT = '…';
exports.LOGICAL_STACK_SEPARATOR_NON_ADJACENT = LOGICAL_STACK_SEPARATOR_NON_ADJACENT;
//# sourceMappingURL=constants.js.map