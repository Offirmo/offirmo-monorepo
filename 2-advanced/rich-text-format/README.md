
A generic, platform independent rich text format.

Can be rendered in ascii, html, react...

See /demos/*


Inspiration:
* https://developer.atlassian.com/cloud/stride/apis/document/structure/
* https://bitbucket.org/atlassian/adf-builder-javascript#readme


Tosort
* https://developer.mozilla.org/en-US/docs/Web/HTML/Element/dl
