import { Enum } from 'typescript-string-enums';
///////
const NodeType = Enum('span', 'br', 'hr', 'ol', 'ul', 'li', 'strong', 'em', 'section', 'heading');
////////////
export { NodeType, };
//# sourceMappingURL=types.js.map