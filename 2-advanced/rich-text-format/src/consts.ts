const LIB = '@offirmo/rich-text-format'

const SCHEMA_VERSION: number = 1

export {
	LIB,
	SCHEMA_VERSION,
}
