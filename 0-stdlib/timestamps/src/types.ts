
type TimestampUTCMs = number

type HumanReadableTimestampUTCMinutes = string
type HumanReadableTimestampUTCMs = string

export {
	TimestampUTCMs,
	HumanReadableTimestampUTCMinutes,
	HumanReadableTimestampUTCMs,
}
