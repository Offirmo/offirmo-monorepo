import { HumanReadableTimestampUTCMs } from './types';
declare function parse_human_readable_UTC_timestamp_ms_v1(tstamp: string): Date;
declare function parse_human_readable_UTC_timestamp_ms(tstamp: HumanReadableTimestampUTCMs): Date;
export { HumanReadableTimestampUTCMs, parse_human_readable_UTC_timestamp_ms_v1, parse_human_readable_UTC_timestamp_ms };
