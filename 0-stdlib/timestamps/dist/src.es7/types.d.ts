declare type TimestampUTCMs = number;
declare type HumanReadableTimestampUTCMinutes = string;
declare type HumanReadableTimestampUTCMs = string;
export { TimestampUTCMs, HumanReadableTimestampUTCMinutes, HumanReadableTimestampUTCMs };
