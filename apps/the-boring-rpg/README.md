## The Boring RPG, reloaded - web version

Source code for the browser game "The Boring RPG, reloaded"

The game is live here: https://www.online-adventur.es/apps/the-boring-rpg/

## Contributing
This app is hosted inside a bolt JavaScript mono-repo. Go clone it full.

## Releasing
```bash
yarn build
(copy to oa TODO make a script!)
```
