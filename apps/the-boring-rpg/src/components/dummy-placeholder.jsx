import React from 'react'

function DummyPlaceholder({name}) {
	return (
		<div>
			Dummy placeholder for: "{name}"
		</div>
	)
}

export default DummyPlaceholder;
