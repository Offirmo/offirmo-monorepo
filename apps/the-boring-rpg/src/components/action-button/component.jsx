import React from 'react'

import { ActionType } from '@oh-my-rpg/state-the-boring-rpg'

/////////////////////

const ACTION_TYPE_TO_CTA = {
	'play': 'Play',
	'equip_item': 'Equip',
	'sell_item': 'Sell',
	'rename_avatar': 'Rename',
	'change_avatar_class': 'Change class',
}
if (Object.keys(ActionType).join(';') !== Object.keys(ACTION_TYPE_TO_CTA).join(';'))
	throw new Error('Internal error: ACTION_TYPE_TO_CTA needs an update!')

/////////////////////

export default ({action, onClick}) => (
	<button className={'tbrpg-action-btn'} onClick={onClick}>
		{ACTION_TYPE_TO_CTA[action.type]}
	</button>
)
