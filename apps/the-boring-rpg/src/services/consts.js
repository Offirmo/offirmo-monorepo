"use strict";

const LIB = 'the-boring-rpg'

const SCHEMA_VERSION = 1

const LS_KEYS = {
	savegame: `${LIB}.savegame`,
}

export {
	LIB,
	SCHEMA_VERSION,
	LS_KEYS,
}
