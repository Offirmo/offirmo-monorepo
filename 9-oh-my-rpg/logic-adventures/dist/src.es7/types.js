import { Enum } from 'typescript-string-enums';
/////////////////////
const CoinsGain = Enum('none', 'small', 'medium', 'big', 'huge');
const AdventureType = Enum('story', 'fight');
/////////////////////
export { CoinsGain, AdventureType, };
/////////////////////
//# sourceMappingURL=types.js.map