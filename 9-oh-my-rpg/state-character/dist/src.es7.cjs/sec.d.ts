import { SoftExecutionContext, SECContext } from '@oh-my-rpg/definitions';
declare function get_SEC(SEC?: SoftExecutionContext): SoftExecutionContext;
export { SoftExecutionContext, SECContext, get_SEC };
