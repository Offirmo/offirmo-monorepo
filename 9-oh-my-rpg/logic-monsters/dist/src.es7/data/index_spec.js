import { expect } from 'chai';
import { ENTRIES } from '.';
describe('🐀 🐉  monster logic - data:', function () {
    it('should have all the expected monsters', () => {
        expect(ENTRIES).to.have.lengthOf(71);
    });
});
//# sourceMappingURL=index_spec.js.map