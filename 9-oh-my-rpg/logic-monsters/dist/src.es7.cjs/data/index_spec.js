"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const _1 = require(".");
describe('🐀 🐉  monster logic - data:', function () {
    it('should have all the expected monsters', () => {
        chai_1.expect(_1.ENTRIES).to.have.lengthOf(71);
    });
});
//# sourceMappingURL=index_spec.js.map