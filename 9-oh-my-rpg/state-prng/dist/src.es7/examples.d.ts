import { State } from './types';
declare const DEMO_STATE: State;
declare const MIGRATION_HINTS_FOR_TESTS: {
    "to_v1": {};
    "to_v2": {};
};
export { DEMO_STATE, MIGRATION_HINTS_FOR_TESTS };
