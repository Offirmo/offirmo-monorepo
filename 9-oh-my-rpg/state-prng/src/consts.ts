const LIB = '@oh-my-rpg/state-prng'

const SCHEMA_VERSION: number = 2

// TODO move in SEC
const DEBUG = true

export {
	LIB,
	SCHEMA_VERSION,
	DEBUG,
}
