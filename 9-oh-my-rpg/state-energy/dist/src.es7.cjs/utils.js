"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function round_float(x, to = 100) {
    return Math.floor(1. * x * to) / (1. * to);
}
exports.round_float = round_float;
//# sourceMappingURL=utils.js.map