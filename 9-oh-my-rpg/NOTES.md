
TOREAD https://josephg.com/blog/fixing-fortnite/

TODO salvage lists from here https://docs.google.com/spreadsheets/d/1FCNPBMZm6tWfeOEtdOvcOrWDRZVqbe1ipzK9eQmNp2o/edit#gid=443281176

Schema:
http://www.vertabelo.com/blog/technical-articles/a-song-of-ice-and-databases-a-game-of-thrones-data-model

Ideas:
* https://www.reddit.com/r/incremental_games/search?q=flair%3A%27MDMonday%27&restrict_sr=on&sort=new&t=all


## Misc / TOSORT

### Interesting reads:
* https://github.com/Offirmo-team/wiki/wiki/RPG
* https://gamedevelopment.tutsplus.com/categories/game-design
  * https://gamedevelopment.tutsplus.com/tutorials/making-difficult-fun-how-to-challenge-your-players--cms-25873
* ;-) https://gamedevelopment.tutsplus.com/articles/3-questions-to-help-you-finish-your-first-game--gamedev-9576
* https://github.com/mozilla/BrowserQuest


Scénario
* https://en.wikipedia.org/wiki/Matter_of_France
** the Twelve Peers of France
** paladins
* https://en.wikipedia.org/wiki/Matter_of_Britain
* https://worldbuilding.stackexchange.com/questions/111349/how-can-a-colony-of-teleporters-make-the-most-money-while-keeping-their-teleport
* https://twitter.com/ptychomancer/status/980968298002006016

TODO omr
* immutabilité https://github.com/mweststrate/immer
* CQRS
  if switch to immutable: change all action parameters to Readonly<State>
* time
  * seasonal content
- non tty
- social/virality server
- stride
- savegame backup
- slack

fantasy metals
* http://tvtropes.org/pmwiki/pmwiki.php/Main/FantasyMetals
* http://metallurgy.wikia.com/wiki/Fantasy_Metals
* http://www.bodycote.com/fictional-metals


Marketing
- news
- live démo

bugs:
- inventory full
- display on windows

TODO: omr
- luck should have an effect: more money, more chance to gain artifacts, etc.
- funny negative adventure
- battles

Otherworlder: https://www.dandwiki.com/wiki/Otherworlder_(5e_Background)


/*
.pushText(''
	+ 'Great sages prophesied your coming,{{br}}'
	+ 'commoners are waiting for their hero{{br}}'
	+ 'and kings are trembling from fear of change...{{br}}'
	+ '…undoubtedly, you’ll make a name in this world and fulfill your destiny!{{br}}'
	+ '{{br}}'
)
.pushStrong('A great saga just started.')
*/


https://github.com/f/graphql.js


Priest = ce qui devrait être
besoin d'un cœur pur sans préjugés
rare, difficile d'accès


Content:
* stories
  * https://ifcomp.org/comp/2017
  * http://mangakakalot.com/chapter/the_great_ruler/chapter_21
* pics
  * https://www.pinterest.com.au/athn1/commoner-for-dd/
  * https://lonewolfangel.deviantart.com/favourites/63788883/Fantasy-Male
  * https://www.pinterest.com.au/msquirewriting/fantasy-art/
  * https://deathbornaeon.deviantart.com/favourites/69459555/Druids-and-Shamans
* world
  * http://forgottenrealms.wikia.com/wiki/Main_Page
  




https://getavataaars.com/?accessoriesType=Wayfarers&avatarStyle=Circle&clotheColor=PastelRed&clotheType=Hoodie&eyeType=Dizzy&eyebrowType=RaisedExcitedNatural&hairColor=Platinum&mouthType=Serious&ref=producthunt&skinColor=Brown&topType=LongHairFroBand

http://paizo.com/pathfinderRPG

http://idlerpg.net/


emotions: https://westus.dev.cognitive.microsoft.com/docs/services/5639d931ca73072154c1ce89/operations/563b31ea778daf121cc3a5fa
    "anger": 0.00300731952,
      "contempt": 5.14648448E-08,
      "disgust": 9.180124E-06,
      "fear": 0.0001912825,
      "happiness": 0.9875571,
      "neutral": 0.0009861537,
      "sadness": 1.889955E-05,
      "surprise": 0.008229999
    



https://games.slashdot.org/story/17/05/29/0632250/esr-announces-the-open-sourcing-of-the-worlds-first-text-adventure
https://gamedevelopment.tutsplus.com/categories/game-design


https://github.com/rangle/typed-immutable-record

http://mewo2.com/notes/naming-language/
http://mewo2.com/notes/terrain/ Generating fantasy maps
https://github.com/dariusk/NaNoGenMo-2015/issues/156

			/*
			const header = _.map(schema.properties, (val, key) => ({
				value: key,
				align : 'left',
			}))
			 const t = Table(header,raw_data, {
				borderColor : "blue",
				paddingBottom : 0,
			})
			console.log(t.render())
			*/
			


Companions

sentences

https://medium.com/the-polymath-project/the-abcs-of-fake-empathy-fdbe4555acc5
Women are taught to use certain phrases to please men and potentially score a future husband:

    Sa: Sasuga! — As expected!
    Shi: Shiranakata! — I didn’t know that!
    Su: Sugoi! — Amazing! Awesome! Wow!
    Se: Sensu ii desu ne! — You have good taste!
    So: Sou nan da! — Really? I see! Is that so?

jungle kid

bride rescued from caravan, doesn't know to whom she was to be married

