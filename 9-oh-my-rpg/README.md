# Oh My RPG ⚔ 👑

**If you're looking for:**
- **the npm rpg** → go here: [the-npm-rpg/README.md](../apps/the-npm-rpg/README.md)
- **the boring rpg** → go here: [the-boring-rpg/README.md](../apps/the-boring-rpg/README.md)


## Intro

My RPG framework, comprised of:
* composable building blocks
* final RPG implementations (using the blocks)

Code is isomorphic, targeting node & browser. This is achieved by being text based with progressive augmentations:
1. pure text
1. text with emojis
1. text with styles: ASCII or CSS
1. pictures (displayable in browser or iterm2)
1. ...


## Usage
Compose and profit !
