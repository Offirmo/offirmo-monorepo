import { Item } from '@oh-my-rpg/definitions';
declare function compare_items(a: Item, b: Item): number;
export { compare_items };
