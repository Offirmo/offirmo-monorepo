const LIB = '@oh-my-rpg/state-the-boring-rpg'

const SCHEMA_VERSION: number = 5
const GAME_VERSION: string = '0.11.0' // for display purpose TODO autoinc?


// XXX moved TODO fix
const URL_OF_WEBSITE = 'https://www.online-adventur.es/the-boring-rpg'
const URL_OF_PRODUCT_HUNT_PAGE = 'https://www.producthunt.com/upcoming/the-npm-rpg'

const URL_OF_REPO = 'https://github.com/Offirmo/offirmo-monorepo/tree/master/apps/the-boring-rpg'
const URL_OF_FORK = 'https://github.com/Offirmo/offirmo-monorepo#fork-destination-box'
const URL_OF_ISSUES = 'https://github.com/Offirmo/offirmo-monorepo/issues'

const URL_OF_REDDIT_PAGE = 'https://www.reddit.com/r/boringrpg/'
// TODO facebook
// TODO instagram
// TODO twitter

export {
	LIB,
	SCHEMA_VERSION,
	GAME_VERSION,
	URL_OF_WEBSITE,
	URL_OF_PRODUCT_HUNT_PAGE,
	URL_OF_REPO,
	URL_OF_FORK,
	URL_OF_ISSUES,
	URL_OF_REDDIT_PAGE,
}
