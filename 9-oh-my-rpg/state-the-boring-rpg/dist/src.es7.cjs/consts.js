"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const LIB = '@oh-my-rpg/state-the-boring-rpg';
exports.LIB = LIB;
const SCHEMA_VERSION = 5;
exports.SCHEMA_VERSION = SCHEMA_VERSION;
const GAME_VERSION = '0.11.0'; // for display purpose TODO autoinc?
exports.GAME_VERSION = GAME_VERSION;
// XXX moved TODO fix
const URL_OF_WEBSITE = 'https://www.online-adventur.es/the-boring-rpg';
exports.URL_OF_WEBSITE = URL_OF_WEBSITE;
const URL_OF_PRODUCT_HUNT_PAGE = 'https://www.producthunt.com/upcoming/the-npm-rpg';
exports.URL_OF_PRODUCT_HUNT_PAGE = URL_OF_PRODUCT_HUNT_PAGE;
const URL_OF_REPO = 'https://github.com/Offirmo/offirmo-monorepo/tree/master/apps/the-boring-rpg';
exports.URL_OF_REPO = URL_OF_REPO;
const URL_OF_FORK = 'https://github.com/Offirmo/offirmo-monorepo#fork-destination-box';
exports.URL_OF_FORK = URL_OF_FORK;
const URL_OF_ISSUES = 'https://github.com/Offirmo/offirmo-monorepo/issues';
exports.URL_OF_ISSUES = URL_OF_ISSUES;
const URL_OF_REDDIT_PAGE = 'https://www.reddit.com/r/boringrpg/';
exports.URL_OF_REDDIT_PAGE = URL_OF_REDDIT_PAGE;
//# sourceMappingURL=consts.js.map