import { State, Adventure } from './types';
declare const DEMO_ADVENTURE_01: Readonly<Adventure>;
declare const DEMO_ADVENTURE_02: Readonly<Adventure>;
declare const DEMO_ADVENTURE_03: Readonly<Adventure>;
declare const DEMO_ADVENTURE_04: Adventure;
declare const DEMO_STATE: Readonly<State>;
declare const OLDEST_LEGACY_STATE_FOR_TESTS: any;
declare const MIGRATION_HINTS_FOR_TESTS: any;
export { DEMO_ADVENTURE_01, DEMO_ADVENTURE_02, DEMO_ADVENTURE_03, DEMO_ADVENTURE_04, DEMO_STATE, OLDEST_LEGACY_STATE_FOR_TESTS, MIGRATION_HINTS_FOR_TESTS };
