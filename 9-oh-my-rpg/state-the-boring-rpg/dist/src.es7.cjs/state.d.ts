import { UUID, Element } from '@oh-my-rpg/definitions';
import { CharacterClass } from '@oh-my-rpg/state-character';
import { State, GainType, Adventure } from './types';
import { Action } from './serializable_actions';
declare function appraise_item(state: Readonly<State>, uuid: UUID): number;
declare function find_element(state: Readonly<State>, uuid: UUID): Element | null;
declare function get_actions_for_element(state: Readonly<State>, uuid: UUID): Action[];
declare function create(): Readonly<State>;
declare function reseed(state: Readonly<State>, seed?: number): Readonly<State>;
declare function play(state: Readonly<State>, explicit_adventure_archetype_hid?: string): Readonly<State>;
declare function equip_item(state: Readonly<State>, uuid: UUID): Readonly<State>;
declare function sell_item(state: Readonly<State>, uuid: UUID): Readonly<State>;
declare function rename_avatar(state: Readonly<State>, new_name: string): Readonly<State>;
declare function change_avatar_class(state: Readonly<State>, new_class: CharacterClass): Readonly<State>;
declare function execute(state: Readonly<State>, action: Action): Readonly<State>;
export { GainType, Adventure, State, create, reseed, play, equip_item, sell_item, rename_avatar, change_avatar_class, execute, appraise_item, find_element, get_actions_for_element };
